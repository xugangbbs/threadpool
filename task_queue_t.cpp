#include "task_queue_t.h"


task_queue_t::task_queue_t(void)
{

}
task_queue_t::~task_queue_t(void)
{

}
void task_queue_t::push_back( task_ptr_t &task )
{
	lock_guard_t lock_guard(queue_mutex);
	task_queue.push(task);
}

bool task_queue_t::pull_front( task_ptr_t &task_ )
{
	lock_guard_t lock_guard(queue_mutex);
	if (task_queue.empty())
	{
		return false;
	}
	task_ = task_queue.front();
	task_queue.pop();
	return true;
}
